---
layout: page
headtouse: posthead
title: Blog
---

# Blog

## Game Design
These are posts written by lead designer Keerthik on approaches and thought processes behind making a game fun.

* [overpowered](overpowered.html)
* [toys -> games](toys.html)
* [make it fun to face too](antifun.html)
