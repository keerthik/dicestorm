---
layout: page
headtouse: abouthead
title: about
---

# Operation Dicestorm

Tactical board game for the new generation of gamers

[Learn more](/game) about the game.

# Team

## Keerthik Omanakuttan
Gameplay designer, fun-nut

Tinkering about every aspect of the game to make it as fun and interesting as possible. Also does random stuff like run the website, write design blog entries, run playtests, and pester people to like the {{ site.ds }} facebook page.

## Philip Chung
Gameplay engineer, makes (physical) and balances (gameplay) things

Sands, saws, lasercuts and more. Besides bringing the game to life, keeps Keerthik's mechanics and themes in check so that the game is playably balanced.

## And Co.
Countless other people have helped us along the way, including:
Chen, Andy B, Alison (art), Colden (design) and tons of awesome folks who playtested with us.