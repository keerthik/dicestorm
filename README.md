Operation Dicestorm
=========

Currently viewable live at http://www.dicestormgame.com

# This Repo
This repo manages our site systems, blog, game information, and the Data-driven Card Framework.
Uses static site generator [jekyll](http://jekyllrb.com/).

# Quick start
```
$ sudo gem install bundler -v 2.4.22
$ sudo bundle install
$ bundle exec jekyll serve
```

# Data-driven Card Framework
The unit cards (found in `/game/units/index.html`) are generated using html/css/yml.
The `/_includes/unittemplate.html` determines all the card layout.
The `/style/ds-unit.css` determines all the card styling.
The `/_data/yml` files contain the data for each unit/card per file.
The `/js/units.js` invokes `html2canvas` to temporarily convert the html cards into canvas elements so that they can be downloaded as images. 

Usually individual unit card images are imported into a document which is then exported as a printable pdf (such as is found in `/game/print/`). 

Note that the css file has additional classes `.printTop` and `.printBottom` which are added to the hexagon because of a bug in how html2canvas renders certain elements.

## TODO
- auto-exporting into pdf files laid out for optimal printing without requiring manual uploads of pdf files
- in-browser customization of units for new pdf exports
- removing styling information from yml files and adding layout logic to interpret that

# About Dicestorm
[Operation Dicestorm](http://www.facebook.com/dicestorm) is a new generation board game with a beautiful laser cut wood board, developed as a hobby by @keerthiko and @philipchung.
